import { useRef, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
const Login = () => {
  const navi = useNavigate();
  const errorRef = useRef();
  const [user, setUser] = useState({
    email: "",
    password: "",
  });
  const [errorMessage, setErrMsg] = useState("");
  // update the input field on type
  // handleChange(event, setState);
  const handleChange = (e, callback) => {
    callback((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))
  }

  const login = async (e) => {
    e.preventDefault();
    if (user.email && user.password) {
      axios
        .post("http://localhost:3001/login/user", user)
        .then(async (response) => {
          if (response.data.user) {
            await setUser((prevState) => ({
              ...prevState,
              email: "",
              password: "",
            }));
            alert(response.data.message);
            localStorage.setItem("token", response.data.user.token);
            navi("/all-details");
          } else {
            await setErrMsg((prevState) => response.data.message);
            await setUser((prevState) => ({
              ...prevState,
              password: "",
            }));
          }
        })
        .catch((err) => {
          
        });
    } else {
      await setErrMsg((prevState) => "Please fill all the fields");
      errorRef.current.style.display = "block";
    }
  };
  return (
    <>
      <div className="login bg-lt-purple">
        <form className="bg-cream auth-form" onSubmit={login}>
          {errorMessage && (
            <div className="alert alert-danger" ref={errorRef}>
              <span
                className="closebtn"
                onClick={(e) => {
                  e.target.parentElement.style.display = "none";
                }}
              >
                &times;
              </span>
              {errorMessage}
            </div>
          )}
          <div className="input-div">
            <label className="black" htmlFor="useremail">
              Email
            </label>
            <input
              className="input-field"
              name="email"
              value={user.email}
              placeholder="Enter Your Email..."
              id="useremail"
              type="text"
              autoComplete="off"
              onChange={(e) =>
                handleChange(e, setUser)
              }
            />
          </div>
          <div className="input-div">
            <label className="black" htmlFor="userpassword">
              Password
            </label>
            <input
              className="input-field"
              name="password"
              value={user.password}
              placeholder="Enter Your Password..."
              id="userpassword"
              type="password"
              autoComplete="off"
              onChange={(e) =>
                handleChange(e, setUser)
              }
            />
          </div>
          <button type="submit" className="auth-button">
            Login
          </button>
          <p onClick={() => navi("/")}>
            New User ! <span className="anchor">Register Yourself</span>
          </p>
        </form>
      </div>
    </>
  );
};

export default Login;
